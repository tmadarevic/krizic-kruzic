﻿namespace krizic_kruzic
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.igrac1 = new System.Windows.Forms.TextBox();
            this.igrac2 = new System.Windows.Forms.TextBox();
            this.kreni = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(12, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Igrač 1:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(12, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Igrač 2:";
            // 
            // igrac1
            // 
            this.igrac1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.igrac1.Location = new System.Drawing.Point(88, 12);
            this.igrac1.Name = "igrac1";
            this.igrac1.Size = new System.Drawing.Size(156, 26);
            this.igrac1.TabIndex = 2;
            // 
            // igrac2
            // 
            this.igrac2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.igrac2.Location = new System.Drawing.Point(88, 48);
            this.igrac2.Name = "igrac2";
            this.igrac2.Size = new System.Drawing.Size(156, 26);
            this.igrac2.TabIndex = 3;
            this.igrac2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.igrac2_KeyPress);
            // 
            // kreni
            // 
            this.kreni.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.kreni.Location = new System.Drawing.Point(195, 80);
            this.kreni.Name = "kreni";
            this.kreni.Size = new System.Drawing.Size(49, 31);
            this.kreni.TabIndex = 4;
            this.kreni.Text = "Kreni";
            this.kreni.UseVisualStyleBackColor = true;
            this.kreni.Click += new System.EventHandler(this.kreni_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(266, 118);
            this.Controls.Add(this.kreni);
            this.Controls.Add(this.igrac2);
            this.Controls.Add(this.igrac1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(282, 156);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(282, 156);
            this.Name = "Form2";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Unesi igrače:";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox igrac1;
        private System.Windows.Forms.TextBox igrac2;
        private System.Windows.Forms.Button kreni;
    }
}